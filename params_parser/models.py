class ParamsParser(object):

    def get_parameters_to_values(self, obj):
        return zip(obj.__code__.co_varnames, obj.__defaults__)

    def get_params(self, obj):
        raise NotImplementedError("This is a call to the base class' %s" % self.get_params.__name__)

    def get_params_with_defaults(self):
        raise NotImplementedError("This is a call to the base class' %s" % self.get_params_with_defaults.__name__)


class ParsedParams(object):
    def __init__(self, parent_obj_name):
        self.parent_obj_name = parent_obj_name
        self.parameter_names = []
        self.parameters_with_defaults = {}
        self.parameters_with_all_available_values = {}


class ScikitParamsParser(ParamsParser):

    def __init__(self):
        super(ScikitParamsParser, self)
        self.params_report = None

    def get_params(self, obj):
        self.params_report = ParsedParams(obj.__name__)
        # check if object has get_params
        if hasattr(obj, "get_params"):
            self.params_report.parameters_with_defaults = obj.get_params()
        else:
            self.params_report.parameters_with_defaults = self.get_parameters_to_values(obj)
        # check the signature and get any defaults

        # check the docstrings and extract available values for each param - if any
        self._parse_doc_string(obj)

    def _parse_doc_string(self, obj):
        doc_str = obj.__doc__
        params_found = False
        parsed_params = {}

        if doc_str:
            # check for Parameters
            # if found,
            lines = doc_str.split("\n")
